**API de gestion de problème technique**

Cette APi permet la création, l'édition et la gestion de problème technique
documentation sur postman:
https://documenter.getpostman.com/view/20204158/UzBju8oG

---

## cloner le répertoire

``git clone https://graquil@bitbucket.org/graquil/p10_raquil_giovanni.git``

## activation de l'environnement virtuel

pour préparer le lancement de API veuillez suivre les instructions suivantes dans un terminal.


2. lancer l'environement virtuel :

    1. sous Windows:
        - ``python -m venv env``
        - ``.\env\Scripts\activate``
   
   2. sous Unix:
      - ``python3 -m venv env``
      - ``source env/bin/activate``

3. ``pip install requirement.txt``.

## lancement du serveur

entrer dans le dossier SoftDesk.

4. `` cd SoftDesk ``

lancer le serveur.

5. `` python manage.py runserver``

en cas de message rouge sur les migrations:

6. `` python manage.py makemigrations core ``
7. `` python manage.py makemigrations api ``
8. `` python manage.py migrate ``