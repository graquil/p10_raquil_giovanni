from django.contrib import admin
from django.urls import path, include
from rest_framework_nested import routers

from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
from api.views import ProjectViewset, IssueViewset, CommentViewset, ContributionViewset
from core.views import UserViewSet
router = routers.SimpleRouter()
router.register(r"signup", UserViewSet, basename="user")
router.register(r'projects', ProjectViewset, basename='projects')


projects_router = routers.NestedSimpleRouter(router, r'projects', lookup='project')
projects_router.register(r'issues', IssueViewset, basename='issues')
projects_router.register(r'users', ContributionViewset, basename='users')

issues_router = routers.NestedSimpleRouter(projects_router, 'issues', lookup='issues')
issues_router.register(r'comments', CommentViewset, basename='comments')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path(r'', include(router.urls)),
    path(r'', include(projects_router.urls)),
    path(r'', include(issues_router.urls)),
]
