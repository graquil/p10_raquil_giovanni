from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from django.db.models import Q

from .models import CustomUser
from api.models import Contribution
from .serializers import RegisterSerializer, CustomUserSerializer
from .permission import IsAdminAuthenticated


class MultipleSerializerMixin:

    detail_serializer_class = None
    create_serializer_class = None

    def get_serializer_class(self):
        if self.action == 'retrieve' and self.detail_serializer_class is not None:
            return self.detail_serializer_class
        return super().get_serializer_class()

    def get_permissions(self):
        permission_classes = [IsAdminAuthenticated]
        return [permission() for permission in permission_classes]


class UserViewSet(MultipleSerializerMixin, ModelViewSet):
    serializer_class = RegisterSerializer
    detail_serializer_class = CustomUserSerializer
    http_method_names = ['post', "delete"]

    queryset = CustomUser.objects.all()

    def create(self, request, *args,  **kwargs):
        user_id = super().create(request).data["id"]
        user = CustomUser.objects.get(pk=user_id)
        return Response({
            "CustomUser": RegisterSerializer(user, context=self.get_serializer_context()).data,
            "message": "CustomUser created Successfully.",
        })

    def destroy(self, request, *args, **kwargs):
        user = self.get_object()
        print(user)
        contributions = Contribution.objects.filter(Q(user_id=user.id) & Q(role="AUTHOR"))
        for contribution in contributions:
            contribution.project.delete()
        super().destroy(request)
        return Response({
            "message": "CustomUser delete Successfully.",
        })
