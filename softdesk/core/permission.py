from rest_framework.permissions import BasePermission


class IsAdminAuthenticated(BasePermission):
    def has_permission(self, request, view):
        if request.method == "POST":
            return True
        else:
            if request.user.is_superuser:
                return True
            return False
