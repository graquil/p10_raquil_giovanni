from django.contrib import admin
from django.contrib.auth.admin import UserAdmin


from . import models
from api.models import Project, Contribution, Issue, Comment


admin.site.register(models.CustomUser, UserAdmin)
admin.site.register(Project)
admin.site.register(Contribution)
admin.site.register(Issue)
admin.site.register(Comment)
