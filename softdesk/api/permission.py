from rest_framework.permissions import BasePermission
from django.db.models import Q

from .models import Contribution


class PermissionMixin:
    def get_contributor(self, project_id):
        list_contributors = []
        contributions = Contribution.objects.filter(project_id=project_id)
        for contribution in contributions:
            list_contributors.append(contribution.user)
        return list_contributors

    def get_project_author(self, project_id, request):
        contributions = Contribution.objects.filter(Q(project_id=project_id) & Q(role="AUTHOR"))
        if request.user == contributions[0].user:
            return True
        return False


class IsAuthorAuthenticated(BasePermission, PermissionMixin):
    def has_permission(self, request, view):
        try:
            return self.get_project_author(view.kwargs['project_pk'], request)

        except KeyError:
            return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        print("obj_type = ", obj._type())
        if obj._type() == "Project":
            return self.get_project_author(obj.id, request)

        elif obj._type() == "Contribution":
            return self.get_project_author(obj.project.id, request)

        else:
            if obj.author == request.user:
                return True
            return False


class IsContributorAuthenticated(BasePermission, PermissionMixin):
    def has_permission(self, request, view):
        try:
            contributors = self.get_contributor(view.kwargs["project_pk"])
            print(contributors)
            if request.user in contributors:
                return True
            return False
        except KeyError:
            return request.user and request.user.is_authenticated

    def has_object_permission(self, request, view, obj):
        print(obj._type())
        if obj._type() in ["Contribution", "Issue"]:
            contributors = self.get_contributor(obj.project.id)

        elif obj._type() == "Comment":
            contributors = self.get_contributor(obj.issue.project.id)
            return True

        elif obj._type() == "Project":
            contributors = self.get_contributor(obj.id)

        if request.user in contributors:
            return True
        return False
