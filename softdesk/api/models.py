from django.db import models
from django.conf import settings

# Create your models here.


class Project(models.Model):
    TYPES = (
            ("BACK-END", "back-end"),
            ("FRONT-END", "front-end"),
            ("iOS", "iOS"),
            ("ANDROID", "Android")
            )

    title = models.fields.CharField(max_length=128, unique=True)
    description = models.fields.TextField(max_length=2048, blank=True)
    type = models.fields.CharField(choices=TYPES, max_length=10)

    def __str__(self):
        return self.title

    def _type(self):
        return "Project"


class Contribution(models.Model):
    ROLE = (
        ('AUTHOR', 'Author'),
        ("CONTRIBUTOR", "contributor")
    )
    user = models.ForeignKey(to=settings.AUTH_USER_MODEL, on_delete=models.CASCADE, related_name="user_role")
    project = models.ForeignKey(to=Project, on_delete=models.CASCADE, related_name="project")
    role = models.fields.CharField(choices=ROLE, max_length=11)

    class Meta:
        unique_together = [("user", "project")]

    def __str__(self):
        return f"{self.user} is {self.role} of {self.project}"

    def _type(self):
        return "Contribution"


class Issue(models.Model):
    PRIORITY = (
        ('LOW', 'faible'),
        ('MEDIUM', 'moyenne'),
        ('HIGH', 'élevée')
    )

    STATUS = (
        ('A FAIRE', 'à faire'),
        ('EN COURS', 'en cours'),
        ('TERMINER', 'terminé')
    )

    BALISE = (
        ('BUG', 'bug'),
        ('AMELIORATION', 'amélioration'),
        ('TACHE', 'tache'),
    )

    title = models.fields.CharField(max_length=128)
    description = models.fields.TextField(max_length=2048, blank=True)
    balise = models.fields.CharField(choices=BALISE, max_length=12)
    priority = models.fields.CharField(choices=PRIORITY, max_length=7)
    project = models.ForeignKey(to=Project, null=True, on_delete=models.CASCADE)
    status = models.fields.CharField(choices=STATUS, max_length=10)
    author = models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE, related_name="author")
    assignee_user = models.ForeignKey(to=settings.AUTH_USER_MODEL,
                                      null=True, on_delete=models.SET_NULL, related_name="assignee_user")
    created_time = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = [("title", "assignee_user")]

    def __str__(self):
        return self.title

    def _type(self):
        return "Issue"


class Comment(models.Model):
    description = models.fields.TextField(max_length=2048, blank=True)
    author = models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True, on_delete=models.CASCADE)
    issue = models.ForeignKey(to=Issue, null=True, on_delete=models.CASCADE, related_name="issues")
    created_time = models.DateTimeField(auto_now_add=True)

    def _type(self):
        return "Comment"
