from rest_framework import serializers

from core.serializers import ResumeCustomUserSerializer
from api.models import Project, Issue, Comment, Contribution


class ProjectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = ["id", "title", "type", "description"]


class ProjectDetailSerializer(serializers.ModelSerializer):
    contributions = serializers.SerializerMethodField()

    class Meta:
        model = Project
        fields = ["id", "title", "description", "type", "contributions"]

    def get_contributions(self, obj):
        """
            permet d'avoir la liste des contributeurs

            paramA: projet dont la méthode est apellé

            returnA: list des contributeurs
        """
        queryset = Contribution.objects.filter(project_id=obj.id)
        serializer = ContributionSerializer(queryset, many=True)
        return serializer.data


class ContributionDetailSerializer(serializers.ModelSerializer):
    user = ResumeCustomUserSerializer()
    project = ProjectSerializer()

    class Meta:
        model = Contribution
        fields = ["id", "user", "project", "role"]


class ContributionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Contribution
        fields = ["id", "user", "project", "role"]


class IssueSerializer(serializers.ModelSerializer):

    class Meta:
        model = Issue
        fields = ["id", "title", "description", "balise", "priority",
                  "project", "status", "author", "assignee_user",
                  "created_time"]

    def validate(self, data):
        """
            permet de valider si l'assignee_user est un contributeur
            renvoie une érreur si ce n'est pas le cas

            paramA: dictionnaire de donnée envoyer par la méthode POST

            returnA: dictionnaire de donnée aprés validation
        """
        project_id = data["project"]
        contributions = Contribution.objects.filter(project_id=project_id)
        list_user_contributor = [contribution.user for contribution in contributions]
        if not data["assignee_user"] in list_user_contributor:
            raise serializers.ValidationError("Assignee user must be a contributor.")
        else:
            return data


class IssueDetailSerializer(serializers.ModelSerializer):
    author = ResumeCustomUserSerializer()
    assignee_user = ResumeCustomUserSerializer()
    project = ProjectSerializer()

    class Meta:
        model = Issue
        fields = ["id", "title", "description", "balise", "priority",
                  "project", "status", "author", "assignee_user",
                  "created_time"]


class CommentSerializer(serializers.ModelSerializer):

    class Meta:
        model = Comment
        fields = ["id", "description", "author", "issue", "created_time"]


class CommentDetailSerializer(serializers.ModelSerializer):
    issue = IssueSerializer()
    author = ResumeCustomUserSerializer()

    class Meta:
        model = Comment
        fields = ["id", "description", "author", "issue", "created_time"]
