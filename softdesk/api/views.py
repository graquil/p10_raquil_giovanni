# Create your views here.
from rest_framework.viewsets import ModelViewSet
from rest_framework.response import Response
from .permission import IsAuthorAuthenticated, IsContributorAuthenticated

from .models import Project, Contribution, Issue, Comment
from .serializers import (ProjectSerializer, ProjectDetailSerializer,
                          ContributionSerializer, ContributionDetailSerializer,
                          IssueSerializer, IssueDetailSerializer,
                          CommentSerializer, CommentDetailSerializer)


class MultipleSerializerMixin:

    detail_serializer_class = None
    create_serializer_class = None

    def get_serializer_class(self):
        if self.action in ['retrieve', 'update'] and self.detail_serializer_class is not None:
            return self.detail_serializer_class
        return super().get_serializer_class()

    def get_permissions(self):
        if self.action in ["update", "destroy"]:
            permission_classes = [IsAuthorAuthenticated]
        else:
            permission_classes = [IsContributorAuthenticated]

        return [permission() for permission in permission_classes]


class ProjectViewset(MultipleSerializerMixin, ModelViewSet):

    serializer_class = ProjectSerializer
    detail_serializer_class = ProjectDetailSerializer

    def get_queryset(self):
        if self.action in ["retrieve", "update", "destroy"]:
            return Project.objects.all()
        else:
            user = self.request.user.id
            contributions = Contribution.objects.filter(user=user)
            projects = [contribution.project for contribution in contributions]
            return projects

    def create(self, request, *args, **kwargs):
        project_id = super().create(request).data['id']
        project = Project.objects.get(pk=project_id)
        serializer_contrib = ContributionSerializer(data={"user": self.request.user.id,
                                                          "project": project.id,
                                                          "role": "AUTHOR"})
        serializer_contrib.is_valid(raise_exception=True)
        self.perform_create(serializer_contrib)

        return Response({
            "project": ProjectDetailSerializer(project, context=self.get_serializer_context()).data,
            "message": "Project Created Successfully.",
        })

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        project = super().update(request, *args, **kwargs)
        return Response({
            "project": project.data,
            "message": "Issues update Successfully.",
        })

    def destroy(self, request, *args, **kwargs):
        project = self.get_object()
        contribution = Contribution.objects.filter(project_id=project)
        contribution.delete()
        project.delete()
        return Response({
            "project": ProjectDetailSerializer(project, context=self.get_serializer_context()).data,
            "message": "Project Delete Successfully.",
        })

    def perform_create(self, instance):
        return instance.save()


class ContributionViewset(MultipleSerializerMixin, ModelViewSet):

    serializer_class = ContributionSerializer
    detail_serializer_class = ContributionDetailSerializer
    http_method_names = ['get', 'post', 'head', "delete"]

    def get_queryset(self):
        return Contribution.objects.filter(project_id=self.kwargs['project_pk'])

    def create(self, request, *args, **kwargs):
        data = request.data
        _mutable = data._mutable
        data._mutable = True
        data["project"] = self.kwargs['project_pk']
        data["role"] = "CONTRIBUTOR"
        data._mutable = _mutable
        contribution_id = super().create(request).data["id"]
        contribution = Contribution.objects.get(pk=contribution_id)
        return Response({
            "Contribution": ContributionSerializer(contribution, context=self.get_serializer_context()).data,
            "message": "Contributor created Successfully.",
        })

    def destroy(self, request, *args, **kwargs):
        super().destroy(request)
        return Response({
            "message": "Contributor delete Successfully.",
        })


class IssueViewset(MultipleSerializerMixin, ModelViewSet):

    serializer_class = IssueSerializer
    detail_serializer_class = IssueDetailSerializer

    def get_queryset(self):
        queryset = Issue.objects.all()
        project_id = self.kwargs['project_pk']
        if project_id is not None:
            queryset = queryset.filter(project_id=project_id)
        return queryset

    def create(self, request, *args, **kwargs):
        data = request.data
        _mutable = data._mutable
        data._mutable = True

        data["project"] = self.kwargs['project_pk']
        data["author"] = self.request.user.id
        data._mutable = _mutable
        issue_id = super().create(request).data["id"]
        issue = Issue.objects.get(pk=issue_id)
        return Response({
            "Issue": IssueDetailSerializer(issue, context=self.get_serializer_context()).data,
            "message": "Issue created Successfully.",
        })

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        issue = super().update(request, *args, **kwargs)
        return Response({
            "issue": issue.data,
            "message": "Issues update Successfully.",
        })

    def destroy(self, request, *args, **kwargs):
        super().destroy(request)
        return Response({
            "message": "Issue delete Successfully.",
        })


class CommentViewset(MultipleSerializerMixin, ModelViewSet):

    serializer_class = CommentSerializer
    detail_serializer_class = CommentDetailSerializer

    def get_queryset(self):
        queryset = Comment.objects.all()
        issue_id = self.kwargs['issues_pk']
        if issue_id is not None:
            queryset = queryset.filter(issue_id=issue_id)
        return queryset

    def create(self, request, *args, **kwargs):
        data = request.data
        _mutable = data._mutable
        data._mutable = True
        data["author"] = self.request.user.id
        data["issue"] = self.kwargs['issues_pk']
        data._mutable = _mutable
        comment_id = super().create(request).data["id"]
        comment = Comment.objects.get(pk=comment_id)
        return Response({
            "comment": CommentDetailSerializer(comment, context=self.get_serializer_context()).data,
            "message": "Comment created Successfully.",
        })

    def update(self, request, *args, **kwargs):
        kwargs['partial'] = True
        comment = super().update(request, *args, **kwargs)
        return Response({
            "comment": comment.data,
            "message": "Issues update Successfully.",
        })

    def destroy(self, request, *args, **kwargs):
        super().destroy(request)
        return Response({
            "message": "Comment delete Successfully.",
        })
